package com.bootcamp.eot;

public class Machine {

    public MachineOutcome getOutcome(Move firstMove, Move secondMove) {
        MachineOutcome machineOutcome = null;
        if (firstMove.equals(Move.COOPERATE) && secondMove.equals(Move.COOPERATE)) {
            machineOutcome = new MachineOutcome(2 ,2);
        } else if (firstMove.equals(Move.CHEAT) && secondMove.equals(Move.COOPERATE)) {
            machineOutcome = new MachineOutcome(3 ,-1);
        } else if (firstMove.equals(Move.COOPERATE) && secondMove.equals(Move.CHEAT)) {
            machineOutcome = new MachineOutcome(-1 ,3);
        }else if (firstMove.equals(Move.CHEAT) && secondMove.equals(Move.CHEAT)) {
            machineOutcome = new MachineOutcome(0, 0);
        }

        return machineOutcome;
    }
}
