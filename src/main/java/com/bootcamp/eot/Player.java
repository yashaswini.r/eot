package com.bootcamp.eot;

public class Player {

    private int score;

    public Player() {
        this.score = 0;
    }

    public int getScore() {
        return this.score;
    }

    public void updateScore(int points) {
        this.score += points;
    }
}
