package com.bootcamp.eot;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MachineTest {

    private Machine machine;

    @Before
    public void setup() {
        machine = new Machine();
    }

    @Test
    public void should_take_a_move_return_outcome_for_cheat_and_cooperate() {
        MachineOutcome machineOutcome = machine.getOutcome(Move.CHEAT, Move.COOPERATE);
        assertEquals(3, machineOutcome.getFirstOutcome());
        assertEquals(-1, machineOutcome.getSecondOutcome());
    }

    @Test
    public void should_take_a_move_return_outcome_for_cooperate_and_cheat() {
        MachineOutcome machineOutcome = machine.getOutcome(Move.COOPERATE, Move.CHEAT);
        assertEquals(-1, machineOutcome.getFirstOutcome());
        assertEquals(3, machineOutcome.getSecondOutcome());
    }

    @Test
    public void should_take_a_move_return_outcome_for_cooperate_and_cooperate() {
        MachineOutcome machineOutcome = machine.getOutcome(Move.COOPERATE, Move.COOPERATE);
        assertEquals(2, machineOutcome.getFirstOutcome());
        assertEquals(2, machineOutcome.getSecondOutcome());
    }

    @Test
    public void should_take_a_move_return_outcome_for_cheat_and_cheat() {
        MachineOutcome machineOutcome = machine.getOutcome(Move.CHEAT, Move.CHEAT);
        assertEquals(0, machineOutcome.getFirstOutcome());
        assertEquals(0, machineOutcome.getSecondOutcome());
    }
}
