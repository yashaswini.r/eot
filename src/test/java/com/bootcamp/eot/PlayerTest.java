package  com.bootcamp.eot;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;


public class PlayerTest {
    private Player player = new Player();

    @Test
    public void should_return_player_score() {
        assertEquals(0, player.getScore());
    }

    @Test
    public void should_increment_player_score_by_2() {
        player.updateScore(2);
        assertEquals(2, player.getScore());
    }
    
    
}
