package com.bootcamp.eot;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MachineOutcomeTest {

    MachineOutcome machineOutcome;

    @Before
    public void setup(){
        machineOutcome = new MachineOutcome(2, 3);
    }

    @Test
    public void should_get_outcome(){
        assertEquals(2, machineOutcome.getFirstOutcome());
        assertEquals(3, machineOutcome.getSecondOutcome());
    }
}
